﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeatoApp

{
    class LidarAngle
    {
        private int distance = 0;
        private int intensity = 0;
        private int errorCodeHEX = 0;

        public LidarAngle(int distance, int intensity, int errorCodeHEX) {
            this.distance = distance;
            this.intensity = intensity;
            this.errorCodeHEX = errorCodeHEX;
        }

        public int Distance { get => distance; set => distance = value; }
        public int Intensity { get => intensity; set => intensity = value; }
        public int ErrorCodeHEX { get => errorCodeHEX; set => errorCodeHEX = value; }
    }
    class LidarData
    {
        private LidarAngle[] lidarDataArray = new LidarAngle[360];
        private int timestamp = 0;

        public LidarData(int timestamp) {
            for (int i = 0; i < 360; i++) {
                lidarDataArray[i] = new LidarAngle(0, 0, 0);
            }
            this.timestamp = timestamp;
        }

        public int Timestamp { get => timestamp; set => timestamp = value; }

        public void SetLidarData(int degrees, int distance, int intensity,int ErrorCodeHEX) {
            this.lidarDataArray[degrees].Distance = distance;
            this.lidarDataArray[degrees].ErrorCodeHEX = ErrorCodeHEX;
            this.lidarDataArray[degrees].Intensity = intensity;
        }
        public void SetLidarData(int degrees, int distance, int intensity)
        {
            this.lidarDataArray[degrees].Distance = distance;
            this.lidarDataArray[degrees].Intensity = intensity;
        }
    }
    class LidarMeasurements
    {
        List<LidarData> measurements;
    }
}
