﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeatoApp

{
    class NeatoBotvac
    {
        private AccelData accelData;//= new AccelData();
        private ChargingData chargeData;
        private LidarMeasurements lidarMeasurements;
        private bool isCharging = false;
        private bool isDocked = false;
        private bool isCleaning = false;

        public bool IsCharging { get => isCharging; set => isCharging = value; }
        public bool IsDocked { get => isDocked; set => isDocked = value; }
        public bool IsCleaning { get => isCleaning; set => isCleaning = value; }

        public NeatoBotvac()
        {
            this.IsCharging = false;
            this.IsDocked = false;
            this.IsCleaning = false;
            this.accelData = new AccelData();
            this.chargeData = new ChargingData();
            
        }
        private string [] formatInput(ref string input)
        {
            string[] inputs = input.Split('\n');
            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i] = inputs[i].Trim();
            }
            return inputs;
        }

        
        public void HandleInputFromNeato(string input) {
            //   Console.WriteLine("untrimmed input:\n" + input);

            //  Console.WriteLine("trimmed input:\n" + trimmedInput);
            string[] inputs = this.formatInput(ref input);
                       
            switch (inputs[0])
            {
                case "a":
                    Console.WriteLine("eat shit");
                    break;
                case "getcharger":
                    Console.WriteLine("data from charger");
                    this.chargeData.SetChargerdata(inputs);
                    //do something
                    break;
                case "getaccel":
                    this.accelData.setAccelData(inputs);
                    Console.WriteLine("data from IMU");
                    //save acceldata
                    break;
                case "getldsscan":
                    Console.WriteLine("data from lidar");
                    //save ldsdata
                    break;
                default:
                    Console.WriteLine("Tom DeLong.gif");
                    break;
            }

        }
    }

    class AccelData
    {
        
        private double pitchDeg = 0.0;
        private double rollDeg = 0.0;
        private double XinG = 0.0;
        private double YinG = 0.0;
        private double ZinG = 0.0;
        private double SumInG = 9.81;

        public double PitchDeg { get => pitchDeg; set => pitchDeg = value; }
        public double RollDeg { get => rollDeg; set => rollDeg = value; }
        public double XinG1 { get => XinG; set => XinG = value; }
        public double YinG1 { get => YinG; set => YinG = value; }
        public double ZinG1 { get => ZinG; set => ZinG = value; }
        public double SumInG1 { get => SumInG; set => SumInG = value; }

        public void setAccelData(string [] input)
        {
            foreach(string i in input)
            {
                string [] temp = i.Split(',');
            //    Console.WriteLine("splitted string to be processed: " + i);
                switch (temp[0])
                {
                    case "PitchInDegrees":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.PitchDeg = Dtemp;

                            break;
                        }
                    case "RollInDegrees":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.RollDeg = Dtemp;

                            break;
                        }
                    case "XInG":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.XinG = Dtemp;

                            break;
                        }
                    case "YInG":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.YinG = Dtemp;

                            break;
                        }
                    case "ZInG":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.ZinG = Dtemp;

                            break;
                        }
                    case "SumInG":
                        {
                            double Dtemp = 0.0;
                            Double.TryParse(temp[1], out Dtemp);
                            this.RollDeg = Dtemp;

                            break;
                        }
                    case "Label":
                    default:
                        //Console.WriteLine("default setacceldata");
                        break;
                }
            }
        }
    }
    class ChargingData
    {
        private int fuelPercent = 0;
        private int chargingActive = 0;
        private int chargingEnabled = 0;
        private int confidentOnFuel = 0;
        private int onReservedFuel = 0;
        private int batteryFailure = 0;
        private int extPwrPresent = 0;
        private int emptyFuel = 0;
        private int battTempCAvg = 0;

        
        public void SetChargerdata(string[] input)  
        {
            foreach (string i in input)
            {
                string[] temp = i.Split(',');
              //  Console.WriteLine("splitted string to be processed: " + i);
                switch (temp[0])
                {
                    case "FuelPercent":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.FuelPercent = Itemp;
 //                           Console.WriteLine(this.FuelPercent);
                            break;
                        }
                        
                    case "ChargingActive":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.ChargingActive = Itemp;
//                            Console.WriteLine(this.ChargingActive);
                            break;
                        }
                        
                    case "ChargingEnabled":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.ChargingEnabled = Itemp;
                            break;
                        }
                        
                    case "ConfidentOnFuel":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.ConfidentOnFuel = Itemp;
                            break;
                        }
                        
                    case "OnReservedFuel":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.OnReservedFuel = Itemp;
                            break;
                        }
                        
                    case "EmptyFuel":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.EmptyFuel = Itemp;
                            break;
                        }
                        
                    case "BatteryFailure":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.BatteryFailure = Itemp;
                            break;
                        }
                        
                    case "ExtPwrPresent":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.ExtPwrPresent = Itemp;
                            break;
                        }
                        
                    case "BattTempCAvg":
                        {
                            int Itemp = 0;
                            Int32.TryParse(temp[1], out Itemp);
                            this.BattTempCAvg = Itemp;
                            break;
                        }
                        
                    case "Label":
                    default:
                        break;
                }
            }
        }
        public int FuelPercent { get => fuelPercent; set => fuelPercent = value; }
        public int ChargingActive { get => chargingActive; set => chargingActive = value; }
        public int ChargingEnabled { get => chargingEnabled; set => chargingEnabled = value; }
        public int ConfidentOnFuel { get => confidentOnFuel; set => confidentOnFuel = value; }
        public int OnReservedFuel { get => onReservedFuel; set => onReservedFuel = value; }
        public int BatteryFailure { get => batteryFailure; set => batteryFailure = value; }
        public int ExtPwrPresent { get => extPwrPresent; set => extPwrPresent = value; }
        public int BattTempCAvg { get => battTempCAvg; set => battTempCAvg = value; }
        public int EmptyFuel { get => emptyFuel; set => emptyFuel = value; }
        
    }
}
